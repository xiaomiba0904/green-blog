# green-blog
python-djanog 写的个人博客网站

搭建在阿里云上

demo：www.greenblog.cn

###Powered by python 3 and django 1.9

* 博客文章 markdown 渲染
* 文章侧栏标签云和阅读量排行
* 文章评论
* 登录注册(完成80%)
* 部署的数据库是Mysql
* 云上的配置为 Nginx + uwsgi + django


![demo](https://github.com/xiaomiba0904/green-blog/blob/master/demo.png)
