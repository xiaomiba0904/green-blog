"""green_blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from blog import views

urlpatterns = [
    url(r'^$', views.index, name='blog_index'),
    url(r'^admin/', admin.site.urls),
    url(r'^signin/', views.sign_in, name='sign_in'),
    url(r'^signup/', views.sign_up, name='sign_up'),
    url(r'^signout/', views.sign_out, name='sign_out'),
    url(r'^about/$', views.about, name='about'),
    url('^blog/archive/(?P<slug>[-\w]+)/$', views.category_archive, name="blog_category_archive"),
    url('^blog/single/(?P<slug>[-\w]+)/$', views.single, name="blog_article_single"),

]
