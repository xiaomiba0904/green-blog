# coding:utf-8
import hashlib
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def encryption(str):
    m = hashlib.md5()
    m.update(str)
    return m.hexdigest()


def paginator_page(article_obj, page, num=5):
    # 分页处理函数
    paginator = Paginator(article_obj, num)
    num_pages = paginator.num_pages
    try:
        return paginator.page(page), num_pages
    except PageNotAnInteger:
        return paginator.page(1), num_pages
    except EmptyPage:
        return paginator.page(num_pages), num_pages
