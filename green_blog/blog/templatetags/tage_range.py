#coding:utf-8
from django import template
register = template.Library()

'''自定义模板过滤器'''
@register.filter(name='range')
def make_range(num):
    return range(1,num+1)