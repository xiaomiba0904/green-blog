# coding:utf-8
from django import forms
from .models import Visitors, Comments


class Nameform(forms.ModelForm):
    class Meta:
        model = Visitors
        fields = ['user_name', 'password', 'user_email']
        widgets = {

            'user_name': forms.TextInput(attrs={
                'class': "form-control",
                'placeholder': 'username'
            }),
            'password': forms.PasswordInput(attrs={
                'type': 'password',
                'class': "form-control",
                'placeholder': 'password'
            }),
            'user_email': forms.EmailInput(attrs={
                'class': 'form-control',
                'placeholder': 'email'
            })

        }


class CommentsForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = ['text', ]
        widgets = {
            'text': forms.Textarea(attrs={
                'class': "form-control",
                'rows': 3,
            })}
